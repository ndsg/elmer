import axios from 'axios';

//Make Type
const makeType = (module) => (type) => `${module}/${type}`;
const type = makeType('categories');

//Make Action Creator
const mac = (type, ...argNames) => ( ...args ) => {
    const action = { type };
    argNames.forEach( (arg, index) => {
        action[argNames[index]] = args[index];
    });
    return action;
}

//Actions
const SET_CATEGORY = type('set_category');
const FETCH_START = type('fetch_start');
const FETCH_SUCCESS = type('fetch_success');
const FETCH_ERROR = type('fetch_error');

//Actions Creators
export const setCategory = mac(SET_CATEGORY, 'payload');
export const fetchStart = mac(FETCH_START);
export const fetchSuccess = mac(FETCH_SUCCESS, 'payload');
export const fetchError = mac(FETCH_ERROR, 'error');

//Make Reducer
const createReducer = (IS, handlers) => (state = IS, action) => {
    if(handlers.hasOwnProperty(action.type)) 
        return handlers[action.type](state, action);
    else
        return state;
}

//Initial State
const initialState = {
    data: [],
    fetched: false,
    fetching: false,
    category: '',
    error: null
};

//Reducers
const fetchStartReduce = (state) => ({ ...state, fetching: true });
const fetchSuccessReduce = (state, action) =>({ ...state, fetching: false, fetched: true, data: action.payload });
const fetchErrorReduce = (state, action) => ({ ...state, fetching: false, error: action.error});
const setCategoryReduce = (state, action) => ({ ...state, category: action.payload });

export default createReducer(initialState, {
    [FETCH_START]: fetchStartReduce,
    [FETCH_SUCCESS]: fetchSuccessReduce,
    [FETCH_ERROR]: fetchErrorReduce,
    [SET_CATEGORY]: setCategoryReduce
});

export const fetch = () => async ( dispatch ) => {
    dispatch(fetchStart());
    try {
        const { data } = await axios.get('/area');
        dispatch(fetchSuccess(data));
    } catch ( error ) {
        dispatch(fetchError(error));
    }
}