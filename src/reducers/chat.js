//Actions
const SHOW_CHAT = 'show_chat';

//Actions Creators
export const showChat = ( ) => ({ type: SHOW_CHAT });

//Initial State
const initialState = {
    chatOpen: false
}

//Reducers
export default ( state = initialState, action) => {
    switch(action.type) {
        case SHOW_CHAT: 
            return { ...state, chatOpen: !state.chatOpen }
        default:
            return state;
    }
}
