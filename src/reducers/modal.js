//Actions
const SHOW_MODAL = 'show_modal';
const HIDE_MODAL = 'hide_modal';

//Actions Creators
export const showModal = ( ) => ({ type: SHOW_MODAL });
export const hideModal = ( ) => ({ type: HIDE_MODAL });

//Initial State
const initialState = {
    open: false
}

//Reducers
export default ( state = initialState, action) => {
    switch(action.type) {
        case SHOW_MODAL: 
            return { ...state, open: true };
        case HIDE_MODAL: 
            return { ...state, open: false };
        default:
            return state;
    }
}
