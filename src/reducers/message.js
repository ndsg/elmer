import axios from 'axios';

//Make Type
const makeType = (module) => (type) => `${module}/${type}`;
const type = makeType('message');

//Make Action Creator
const mac = (type, ...argNames) => ( ...args ) => {
    const action = { type };
    argNames.forEach( (arg, index) => {
        action[argNames[index]] = args[index];
    });
    return action;
}

//Actions
const WRITTING = type('writting');
const SEND_MESSAGE_USER = type('send_message_user');
const FETCH_START = type('fetch_start');
const FETCH_SUCCESS = type('fetch_success');
const FETCH_ERROR = type('fetch_error');
const PREVIUS_MESSAGE = type('previus_message');
const DATA_PREVIUS = type('data_previus');

//Actions Creators
export const writting = mac(WRITTING, 'payload');
export const sendMessageUser = mac(SEND_MESSAGE_USER, 'payload');
export const fetchStart = mac(FETCH_START);
export const fetchSuccess = mac(FETCH_SUCCESS, 'payload');
export const fetchError = mac(FETCH_ERROR, 'error');
export const previusMessage = mac(PREVIUS_MESSAGE);
export const dataPrevius = mac(DATA_PREVIUS);

//Make Reducer
const createReducer = (IS, handlers) => (state = IS, action) => {
    if(handlers.hasOwnProperty(action.type)) 
        return handlers[action.type](state, action);
    else
        return state;
}

//Initial State
const initialState = {
    data: [],
    fetched: false,
    fetching: false,
    error: null,
    userMessage: '',
    writting: false, 
    previus: false
};

//Reducers
const writtingReduce = (state, action) => ({ ...state, writting: action.payload });
const fetchStartReduce = (state) => ({ ...state, fetching: true, writting: true, fetched: false });
const fetchSuccessReduce = (state, action) =>({ ...state, fetching: false, fetched: true, writting: false, data: [ ...state.data, ...action.payload ] });
const fetchErrorReduce = (state, action) => ({ ...state, fetching: false, error: action.error});
const sendMessageUserReduce = (state, action) => ({ ...state, data: [ ...state.data, action.payload ] } );
const previusMessageReduce = (state) => ({ ...state, previus: !state.previus })
const dataPreviusReduce = (state) => ({ ...state, data: [...state.data.slice(0, state.data.length - 2)] })

export default createReducer(initialState, {
    [FETCH_START]: fetchStartReduce,
    [FETCH_SUCCESS]: fetchSuccessReduce,
    [FETCH_ERROR]: fetchErrorReduce,
    [SEND_MESSAGE_USER]: sendMessageUserReduce,
    [WRITTING]: writtingReduce,
    [PREVIUS_MESSAGE]: previusMessageReduce,
    [DATA_PREVIUS]: dataPreviusReduce
});

export const fetch = (resource) => async ( dispatch ) => {
    dispatch(fetchStart());
    try {
        const { data: { msg, transmitter, options, next } } = await axios.get(resource);
        setTimeout( () => {
            dispatch(fetchSuccess([{msg, transmitter, options, next}]));
        }, 1000)
    } catch ( error ) {
        dispatch(fetchError(error));
    }
}
