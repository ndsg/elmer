import axios from 'axios';

//Make Type
const makeType = (module) => (type) => `${module}/${type}`;
const type = makeType('settings');

//Make Action Creator
const mac = (type, ...argNames) => ( ...args ) => {
    const action = { type };
    argNames.forEach( (arg, index) => {
        action[argNames[index]] = args[index];
    });
    return action;
}

//Actions
const SET_IMAGE_PROFILE = type('set_image_profile');
const SET_CHAT = type('set_chat');
const SET_THEME = type('set_theme');
const FETCH_START = type('fetch_start');
const FETCH_SUCCESS = type('fetch_success');
const FETCH_ERROR = type('fetch_error');

//Actions Creators
export const setImageProfile = mac(SET_IMAGE_PROFILE, 'payload');
export const setChat = mac(SET_CHAT, 'payload');
export const setTheme = mac(SET_THEME, 'payload');
export const fetchStart = mac(FETCH_START);
export const fetchSuccess = mac(FETCH_SUCCESS, 'payload');
export const fetchError = mac(FETCH_ERROR, 'error');

//Make Reducer
const createReducer = (IS, handlers) => (state = IS, action) => {
    if(handlers.hasOwnProperty(action.type)) 
        return handlers[action.type](state, action);
    else
        return state;
}

//Initial State
const initialState = {
    imgProfile: true, 
    chat: true, 
    theme: 'light',
    fetched: false,
    fetching: false,
    error: null
};

//Reducers
const setImageProfileReduce = (state, action) => ({ ...state, imgProfile: action.payload });
const setChatReduce = (state, action) => ({ ...state, chat: action.payload });
const setThemeReduce = (state, action) => ({ ...state, theme: action.payload });
const fetchStartReduce = (state) => ({ ...state, fetching: true });
const fetchSuccessReduce = (state, action) => ({ 
    ...state, 
    fetching: false, 
    fetched: true, 
    imgProfile: action.payload.imgProfile,
    chat: action.payload.chat,
    theme: action.payload.theme
});
const fetchErrorReduce = (state, action) => ({ ...state, fetching: false, error: action.error});

export default createReducer(initialState, {
    [SET_IMAGE_PROFILE]: setImageProfileReduce,
    [SET_CHAT]: setChatReduce,
    [SET_THEME]: setThemeReduce,
    [FETCH_START]: fetchStartReduce,
    [FETCH_SUCCESS]: fetchSuccessReduce,
    [FETCH_ERROR]: fetchErrorReduce,
});

export const getTheme = () => async ( dispatch ) => {
    dispatch(fetchStart());
    try {
        const { data: { imgProfile, chat, theme } } = await axios.get('/settings');
        setTimeout( () => {
            dispatch(fetchSuccess({imgProfile, chat, theme}));
        }, 1000)
    } catch ( error ) {
        dispatch(fetchError(error));
    }
}
