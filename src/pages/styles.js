import styled from 'styled-components';
import Container from 'react-bootstrap/Container';
import { Link } from 'react-router-dom';

export const Title = styled.h1`
    font-family: 'Bold';
    text-transform: uppercase;
    letter-spacing: 2px;
`;

export const Content = styled(Container)`
    flex-wrap: wrap;
    min-height: calc(100vh - 100px - 5%);
`;

export const Box = styled(Link)`
    width: 300px;
    height: 300px;
    padding: 2%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    margin: 5%;
    background-color: ${ props => props.theme.colors.primary };
    border: 2px solid ${ props => props.theme.colors.secondary };
    border-radius: 15px;
    transition: .5s;
    &:hover{
        transform: scale(1.1);
        cursor: pointer;
        background-color: ${ props => props.theme.colors.secondary };
        border: 2px solid #FFF;
    }
`;

export const BoxText = styled.h1`
    font-family: 'Bold';
    text-transform: uppercase;
    letter-spacing: 2px;
    margin-top: 5%;
`;