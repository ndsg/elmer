import React from 'react';
import { connect } from 'react-redux';
import Row from 'react-bootstrap/Row';
import Tab from 'react-bootstrap/Tab';
import { ContainerTabMenu, CheckBox, Stars, SearchInput, Label, TextArea, Button } from './../components';
import { ContainerContent, ContainerTabPane, TitleTab } from './styles/settings';
import './styles/styles.css';
import { setImageProfile, setChat, setTheme } from './../reducers/settings';
import { MENUS, THEMES } from './../data';

const Settings = ({ imgProfile, setImageProfile, chat, setChat, setTheme }) => {

    return (
        <ContainerContent fluid >
            <Tab.Container id="left-tabs-example" defaultActiveKey="first">
                <Row className="justify-content-md-center">
                    <ContainerTabMenu 
                        menus={MENUS} 
                    />
                    <ContainerTabPane md={7}>
                        <Tab.Content>
                            <Tab.Pane eventKey="first">
                                <TitleTab className="mb-3">Mi Perfil</TitleTab>
                                <CheckBox
                                    id="checkProfileImg"
                                    type="checkbox"
                                    label="Mostrar mi foto de perfil"
                                    initialState={imgProfile}
                                    action={setImageProfile}
                                />
                            </Tab.Pane>
                            <Tab.Pane eventKey="second">
                                <TitleTab className="mb-3">Elmer</TitleTab>
                                <CheckBox 
                                    id="checkChat"
                                    type="checkbox"
                                    label="Mostrar Elmer chatbot"
                                    initialState={chat}
                                    action={setChat}
                                />
                            </Tab.Pane>
                            <Tab.Pane eventKey="third">
                                <TitleTab className="mb-3">Temas</TitleTab>
                                <Label 
                                    text="Selecciona un tema:"
                                />
                                <SearchInput 
                                    placeholder="Tema" 
                                    options={THEMES}
                                    action={setTheme}
                                />
                            </Tab.Pane>
                            <Tab.Pane eventKey="fourth">
                                <TitleTab className="mb-3">Evaluación</TitleTab>
                                <Label 
                                    text="¿Cómo calificarías el Sistema Atlas?"
                                />
                                <Stars 
                                    action={ {} }
                                />
                                <Label 
                                    text="Déjanos tu comentario"
                                />
                                <TextArea 
                                    placeholder="Ingresa un comentario . . ."
                                />
                                <Button size="lg" className="mt-3" text="Enviar comentarios" />
                            </Tab.Pane>
                        </Tab.Content>
                    </ContainerTabPane>
                </Row>
            </Tab.Container>
        </ContainerContent>
    );

}

const mapStateToProps = ({ settings: { imgProfile, chat } }) => {
    return { imgProfile, chat };
}

const mapDispatchToProps = ( dispatch ) => ({
    setImageProfile: ( status ) => dispatch(setImageProfile(status)),
    setChat: ( status ) => dispatch(setChat(status)),
    setTheme: ( theme ) => dispatch(setTheme(theme))
})

export default connect(mapStateToProps, mapDispatchToProps)(Settings);