import React from 'react';
import Row from 'react-bootstrap/Row';
import { Content, Box, BoxText, Title } from './styles';
import { FiCodesandbox, FiFileText } from 'react-icons/fi';

export default () => {

    return (
        <Content fluid>
            <Row className="justify-content-md-center" style={{ marginTop: '5%'}}>
                <Title>¿Que deseas revisar?</Title>
            </Row>
            <Row className="justify-content-md-center">
                <Box to="/apps" style={{ color: 'inherit', textDecoration: 'inherit'}}>
                    <FiCodesandbox size={100} />
                    <BoxText>Aplicativos</BoxText>
                </Box>
                <Box style={{ color: 'inherit', textDecoration: 'inherit'}}>
                    <FiFileText size={100} />
                    <BoxText>Reportes</BoxText>
                </Box>
            </Row>
        </Content>
    );

}