import React from 'react';
import { ChatButton, Chat, Carousel, Search, Modal } from './../components';
import { connect } from 'react-redux';
import { showChat } from '../reducers/chat';
import Container from 'react-bootstrap/Container';

const Home = ({ showChat, chat }) => {

    return (
        <Container fluid >
            <Search />
            <Carousel />
            <Chat />
            {
                chat &&
                    <ChatButton onClick={showChat} />
            }
            <Modal />
        </Container>
    );

}

const mapStateToProps = ({ messages, settings: { chat } }) => {
    return { data: messages, chat };
}

const mapDispatchToProps = ( dispatch ) => ({
    showChat: () => dispatch(showChat())
})

export default connect(mapStateToProps, mapDispatchToProps)(Home);