import styled from 'styled-components';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';

export const ContainerContent = styled(Container)`
    min-height: calc(100vh - 100px);
    padding: 5%;
`;

export const ContainerTabPane = styled(Col)`
    background-color: ${ props => props.theme.colors.primary };
    padding: 2%;
    border-top-right-radius: 15px; 
    border-bottom-right-radius: 15px;
`;

export const TitleTab = styled.h1`
    text-transform: uppercase;
    font-family: 'Bold';
    letter-spacing: 2px;
`;