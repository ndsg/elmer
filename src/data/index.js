import { FiUser, FiMessageCircle, FiHelpCircle } from 'react-icons/fi';
import { IoColorPaletteOutline } from 'react-icons/io5';

export const MENUS = [
    { icon: <FiUser size={50} />, title: 'Mi Perfil', eventKey: 'first' },
    { icon: <FiMessageCircle size={50} />, title: 'Elmer', eventKey: 'second' },
    { icon: <IoColorPaletteOutline size={50} />, title: 'Temas', eventKey: 'third' },
    { icon: <FiHelpCircle size={50} />, title: 'Evaluación', eventKey: 'fourth' }
];

export const THEMES = [
    { label: 'Light (Default)', value: 'light' }, 
    { label: 'Dark', value: 'dark'},
    { label: 'Chill', value: 'chill'},
    { label: 'Gold', value: 'gold'},
]