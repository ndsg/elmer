import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { Menu, Home, Settings } from './../pages';

export default () => {

    return (
        <Switch>
            <Route path="/" exact component={Menu} />
            <Route path="/apps" component={Home} />
            <Route path="/settings" component={Settings} />
        </Switch>
    );

}