export default {
    title: 'light',
    colors: {
        primary: '#E5E5E5',
        secondary: '#B7D500',
        background: '#EAEFFF',
        color: '#000'
    }
}