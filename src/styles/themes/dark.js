export default {
    title: 'dark',
    colors: {
        primary: '#333',
        secondary: '#B7D500',
        background: '#222',
        color: '#FFF'
    }
}