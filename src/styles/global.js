import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
    *{
        padding: 0;
        margin: 0;
    }

    body{
        background-color: ${ props => props.theme.colors.background };
        font-size: 18px;
        color : ${ props => props.theme.colors.color };
        font-family: 'Regular';
    }
`;