import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { SCSearchInput } from './styles';
import PropTypes from 'prop-types';

const SearchInput = ({ placeholder, options, action }) => {

    return (
        <Row>
            <Col>
                <SCSearchInput 
                    placeholder={placeholder}
                    options={options} 
                    classNamePrefix="react-select" 
                    className='react-select-container'
                    onChange={ ({value}) => action(value) }    
                />
            </Col>
        </Row>
    );

}

SearchInput.propTypes = {
    placeholder: PropTypes.string.isRequired,
    options: PropTypes.array.isRequired,
    action: PropTypes.func.isRequired
}

export default SearchInput;