import styled from 'styled-components';
import Select from 'react-select';

export const SCSearchInput = styled(Select)`
    .react-select__single-value {
        color: ${ props => props.theme.colors.color };
    }
    .react-select__control {
        width: 50%;
        border-radius: none;
        outline: none;
        border: 2px solid ${ props => props.theme.colors.secondary };
        border-radius: 15px;
        background-color: transparent;
        padding: 1%;
        font-family: 'Regular';
        box-shadow: none;
    }
    .react-select__control:hover{
        border-color: ${ props => props.theme.colors.secondary };
    }
    .react-select__menu{
        width: 50%;
        background-color: ${ props => props.theme.colors.primary };
    }
    .react-select__option {
        font-family: 'Regular';
        background-color: ${ props => props.theme.colors.primary };
    }
    .react-select__option:hover{
        background-color: ${ props => props.theme.colors.secondary };
    }
`;