import styled from 'styled-components';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';

export const ModalHeader = styled(Modal.Header)`
    background-color: ${ props => props.theme.colors.background };
    border-bottom: 2px solid #B7D500;
    align-items: center;
`;

export const ModalBody = styled(Modal.Body)`
    background-color: ${ props => props.theme.colors.background };
    border-bottom: 2px solid #B7D500;
`;

export const ModalFooter = styled(Modal.Footer)`
    background-color: ${ props => props.theme.colors.background };
    border-top: none;
    border-bottom: none;
`;

export const ModalTitle = styled(Modal.Title)`
    font-family: 'Bold';
    letter-spacing: 2px;
    text-transform: uppercase;
    color: #B7D500;
`;

export const AppItem = styled.li`
    font-family: 'Regular';
    margin: 2% 0;
    letter-spacing: 2px;
    text-transform: uppercase;
    list-style: none;
    margin-left: 5%;
    &:hover{
        font-family: 'Bold';
        color: #B7D500;
        transition: all .5s ease-in-out;
        cursor: pointer;
    }
    &:before{
        content: "• ";
        color: #B7D500;
        width: 2vw;
    }
`;

export const SmallTitle = styled.small`
    font-family: 'Regular';
    font-size: 14px;
    letter-spacing: 2px;
    text-transform: uppercase;
    color: #B7D500;
`;

export const ButtonClose = styled(Button)`
    outline: none;
    font-family: 'Bold';
    background-color: #B7D500;
    border: none;
    width: 30%;
    text-transform: uppercase;
    letter-spacing: 2;
    &:hover{
        background-color: #B7D500;
    }
    &:active{
        background-color: #B7D500;
    }
    &:focus{
        background-color: #B7D500;
    }
`;