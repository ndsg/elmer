import React from 'react';
import { connect } from 'react-redux';
import Modal from 'react-bootstrap/Modal';
import { ModalHeader, ModalBody, ModalFooter, ModalTitle, AppItem, ButtonClose, SmallTitle } from './styles';
import { hideModal } from './../../reducers/modal';
import { fetch } from './../../reducers/apps';
import PropTypes from 'prop-types';

const PopUp = ({ apps, modal, hideModal, category, fetch, fetched }) => {

    if(!fetched)
        fetch();

    return (
        <>
            <Modal show={modal} onHide={hideModal} size="lg" centered>
                <ModalHeader>
                    <ModalTitle>Aplicativos</ModalTitle>
                    <SmallTitle> {category} </SmallTitle>
                </ModalHeader>
                <ModalBody>
                    {
                        apps.map( (item) => 
                            <a 
                                href="https://www.google.cl" 
                                style={{ textDecoration: 'none', color: 'inherit' }}
                                key={item.id.toString()}
                            >
                                <AppItem> {item.name} </AppItem>
                            </a>
                        )
                    }
                </ModalBody>
                <ModalFooter>
                    <ButtonClose onClick={() => hideModal()} size="lg">
                        Cerrar
                    </ButtonClose>
                </ModalFooter>
            </Modal>
        </>
    );

}

const mapStateToProps = ({ apps: { filter, fetched }, modal: { open }, categories: { category } }) => {
    return { modal: open, apps: filter, category, fetched };
}

const mapDispatchToProps = ( dispatch ) => ({
    hideModal: () => dispatch(hideModal()),
    fetch: () => dispatch(fetch())
})

PopUp.propTypes = {
    apps: PropTypes.array.isRequired, 
    modal: PropTypes.bool.isRequired, 
    hideModal: PropTypes.func.isRequired, 
    category: PropTypes.string.isRequired, 
    fetch: PropTypes.func.isRequired, 
    fetched: PropTypes.bool.isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(PopUp);