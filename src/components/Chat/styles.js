import styled from 'styled-components';
import { shade } from 'polished';

export const Container = styled.div`
    margin: auto;
    min-height: 70vh;
    min-width: 25vw;
    max-width: 25vw;
    border-radius: 25px;
    position: fixed;
    top: 16%;
    right: 1%;
`;

export const Header = styled.div`
    min-height: 8vh;
    background-color: ${ props => props.theme.colors.secondary };
    border-top-left-radius: 25px;
    border-top-right-radius: 25px;
    display: flex;
    justify-content: flex-start;
    align-items: center;
`;

export const Body = styled.div`
    min-height: 62vh;
    max-height: 62vh;
    background-color: ${ props => props.theme.colors.primary };
    overflow-y: auto;
    padding: 5% 5%;
    padding-bottom: 10%;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    border-bottom-left-radius: 25px;
    border-bottom-right-radius: 25px;
`;

export const BotName = styled.h1`
    margin-left: 5%;
    font-family: 'Bold';
    font-size: 30px;
    letter-spacing: 3px;
    text-transform: uppercase;
`;

export const Message = styled.textarea`
    width: 85%;
    padding: 1% 2%;
    border-radius: 10px;
    border: none;
    outline: none;
    resize: none;
    background-color: ${ shade(.1, '#FFF') };
    font-family: 'Regular';
    white-space: pre-wrap;
`;

export const ContainerMessage = styled.div`
    padding: 2%;
    background-color:  ${ props => props.align === 'left' ? props.theme.colors.secondary : shade(.2, props.theme.colors.secondary) };
    width: 60%;
    border-radius: 15px;
    margin-top: 2%;
    float: ${ props=> props.align };
`;

export const ContainerOption = styled.div`
    padding: 2%;
    background-color: ${ props => props.theme.colors.primary };
    width: 60%;
    border-radius: 15px;
    margin-top: 2%;
    float: left;
    cursor: pointer;
    &:hover{
        background-color: ${ props => props.theme.colors.secondary };
    }
`;

export const TextMessage = styled.div`
    font-family: 'Regular';
`;