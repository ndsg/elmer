import React, { useEffect } from 'react';
import { Container, Header, Body, BotName, ContainerMessage, TextMessage, ContainerOption } from './styles';
import { Writting } from './../';
import imgChat from './../../img/chat.png';
import { BiArrowBack } from 'react-icons/bi';
import { connect } from 'react-redux';
import { fetch, sendMessageUser, dataPrevius, previusMessage } from '../../reducers/message';
import PropTypes from 'prop-types';

const Chat = ({ chatOpen, data, fetch, sendMessageUser, previus, dataPrevius, fetched }) => {

    useEffect( () => {
        const objDiv = document.getElementById('chat');
        objDiv.scrollTop = objDiv.scrollHeight;
    })

    useEffect( () => {
        fetch('/mensaje');
    }, [])

    const previusMessage = () => {
        previus();
        dataPrevius()
        previus();
    }

    return (
        <Container hidden={!chatOpen}>
            <Header>
                <img src={imgChat} style={{ width: '50px', marginLeft: '5%' }} />
                <BotName>Elmer JR</BotName>
            </Header>
            <Body id="chat">
                <div>
                    {
                        data.map( item => 
                            <>
                                <ContainerMessage align={ (item.transmitter === 'b') ? 'left' : 'right' } >
                                    <TextMessage>
                                        {item.msg}
                                    </TextMessage>
                                </ContainerMessage>
                                { 
                                    item.options &&
                                    item.options.map( option => 
                                        <ContainerOption onClick={ () => { sendMessageUser({ msg: option.name, transmitter: 'u' }); fetch(`/${item.next}/${option.id}`) }}>
                                            <TextMessage>
                                                {option.name}
                                            </TextMessage>
                                        </ContainerOption>
                                    )    
                                }
                            </>                            
                        )
                    }
                    {
                        data.length !== 1 && fetched &&
                            <ContainerOption>
                                <TextMessage onClick={previusMessage}>
                                    <BiArrowBack size={25} style={{ marginRight: '2%' }} />
                                    Volver atrás
                                </TextMessage>
                            </ContainerOption>
                    }
                </div>
                <Writting />
            </Body>
        </Container>
    );
}

const mapStateToProps = ({ chat: { chatOpen }, messages: { data, fetched } }) => {
    return { chatOpen, data, fetched };
} 

const mapDispatchToProps = ( dispatch ) => ({
    fetch: (resource) => dispatch(fetch(resource)),
    sendMessageUser: ( message ) => dispatch(sendMessageUser(message)),
    previus: () => dispatch(previusMessage()),
    dataPrevius: () => dispatch(dataPrevius())
})

Chat.propTypes = {
    chatOpen: PropTypes.bool.isRequired, 
    data: PropTypes.array.isRequired, 
    fetch: PropTypes.func.isRequired, 
    sendMessageUser: PropTypes.func.isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat);