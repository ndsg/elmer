import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ReactStars from 'react-stars';
import PropTypes from 'prop-types';

const Stars = ({ action }) => {

    return (
        <Row>
            <Col>
                <ReactStars
                    count={5}
                    /* onChange={ratingChanged} */
                    edit={true}
                    size={50}
                    color2={'#B7D500'} 
                />
            </Col>
        </Row>
    );

}

Stars.propTypes = {
    action: PropTypes.func.isRequired
}

export default Stars;