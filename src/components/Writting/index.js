import React from 'react';
import { connect } from 'react-redux';
import { Writting, WrittingText } from './styles';
import imgUser from './../../img/user.png';
import PropTypes from 'prop-types';

const Write = ({ writting }) => {

    return (
        <Writting hidden={!writting}>
            <div> 
                <img src={imgUser} width="25px" style={{ marginRight: '5px' }} /> 
            </div>
            <div> 
                <WrittingText>Escribiendo . . .</WrittingText> 
            </div>
        </Writting>
    );

}

const mapStateToProps = ({ messages: { writting } }) => {
    return { writting };
}

Write.propTypes = {
    writting: PropTypes.bool.isRequired
}

export default connect(mapStateToProps)(Write);