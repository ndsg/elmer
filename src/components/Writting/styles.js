import styled from 'styled-components';

export const Writting = styled.div`
    /* margin-top: 2vh; */
    display: flex;
    justify-content: flex-end;
    align-items: center;
    align-content: flex-end;
    padding-bottom: 2vh;
`;

export const WrittingText = styled.label`
    text-align: right;
    font-family: 'Italic';
    letter-spacing: 2px;
    font-size: 14px;
    width: 100%;
`;