import styled from 'styled-components';
import Button from 'react-bootstrap/Button';

export const ChatButton = styled(Button)`
    background-color: ${ props => props.theme.colors.secondary };
    padding: 20px;
    border: none;
    outline: none;
    border-radius: 50px;
    right: 30px;
    bottom: 30px;
    position: fixed;
    &:hover{
        transform: scale(1.2);
        background-color: ${ props => props.theme.colors.secondary };
        transition: all 1s ease-in-out;
    }
    &:focus{
        transform: scale(1.2);
        background-color: ${ props => props.theme.colors.secondary };
        outline: none !important;
        box-shadow: none;
    }
    &:active {
        outline: none !important;
        box-shadow: none;
    }
`;