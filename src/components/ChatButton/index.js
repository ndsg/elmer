import React from 'react';
import { connect } from 'react-redux';
import { ChatButton } from './styles';
import { FiMessageCircle, FiXCircle } from 'react-icons/fi';
import PropTypes from 'prop-types';

const Button = ({ onClick, chatOpen }) => {

    return (
        <ChatButton onClick={onClick}> 
        {
            chatOpen ? <FiXCircle size={50} /> : <FiMessageCircle size={50} />
        }
        </ChatButton >
    );

}

const mapStateToProps = ({ chat: { chatOpen }}) => {
    return { chatOpen };
} 

ChatButton.propTypes = {
    onClick: PropTypes.func.isRequired,
    chatOpen: PropTypes.bool
}

export default connect(mapStateToProps)(Button)