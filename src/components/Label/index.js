import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { SCLabel } from './styles';
import PropTypes from 'prop-types';

const Label = ({ text }) => {

    return (
        <Row>
            <Col>
                <SCLabel>{ text }</SCLabel>
            </Col>
        </Row>
    );

}

Label.propTypes = {
    text: PropTypes.string.isRequired
}

export default Label;