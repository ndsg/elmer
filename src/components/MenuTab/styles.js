import styled from 'styled-components';
import Nav from 'react-bootstrap/Nav';

export const SCMenuItem = styled(Nav.Item)`
    margin-top: 10%;
    margin-bottom: 10%;
    &:focus {
        background-color: red !important;
    }
    .active {
        background-color: ${ props => props.theme.colors.secondary } !important;
    }
`;

export const MenuTab = styled(Nav.Link)`
    font-size: 18px;
    display: flex; 
    flex-direction: column;
    justify-content: center; 
    align-items: center;
    color: inherit;
    text-decoration: none;
    &:hover{
        background-color: ${ props => props.theme.colors.secondary } !important;
        color: inherit;
    }
    &:active {
        background-color: ${ props => props.theme.colors.secondary } !important;
    }
    &:focus{
        background-color: ${ props => props.theme.colors.secondary } !important;
    }
`;