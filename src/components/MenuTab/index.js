import React from 'react'
import { SCMenuItem, MenuTab } from './styles';
import PropTypes from 'prop-types';

const MenuItem = ({ icon, title, eventKey }) => {

    return (
        <SCMenuItem>
            <MenuTab eventKey={eventKey}> 
                {icon} { title }
            </MenuTab>
        </SCMenuItem>
    );

}

MenuItem.propTypes = {
    icon: PropTypes.element.isRequired,
    title: PropTypes.string.isRequired,
    eventKey: PropTypes.string.isRequired
}

export default MenuItem;