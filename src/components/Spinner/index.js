import React from 'react';
import BeatLoader from 'react-spinners/BeatLoader';
import { css } from "@emotion/core";

const override = css`
    margin: auto;
`;

const Spinner = () => {

    return (
        <BeatLoader color={'#B7D500'} loading={true} css={override} size={30} margin={10} />
    );

}

export default Spinner;