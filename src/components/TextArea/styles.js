import styled from 'styled-components';

export const TextArea = styled.textarea`
    resize: none;
    width: 50%;
    border: none;
    outline: none;
    box-shadow: none;
    padding: 2%;
    border-radius: 15px;
    min-height: 20vh;
    background-color: transparent;
    color: inherit;
    border: 2px solid ${ props => props.theme.colors.secondary };;
    margin-top: 2vh;
`;