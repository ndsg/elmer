import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { TextArea } from './styles';

export default ({ placeholder }) => {

    return (
        <Row>
            <Col>
                <TextArea placeholder={placeholder} />
            </Col>
        </Row>
    )

}