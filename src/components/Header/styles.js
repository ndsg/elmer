import styled from 'styled-components';
import Dropdown from 'react-bootstrap/Dropdown';

export const Container = styled.div`
    height: 100px;
    background-color: ${ props => props.theme.colors.primary };
    color: #fff;
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 0 30px;
    width: 100%;
`;

export const ButtonDrop = styled(Dropdown.Toggle)`
    background-color: ${ props => props.theme.colors.secondary };
    border: none; 
    box-shadow: none,;
    outline: none;
    font-family: 'Bold';
    &:hover{
        background-color: ${ props => props.theme.colors.secondary };
    }
    &:active{
        background-color: ${ props => props.theme.colors.secondary };
    }
`;