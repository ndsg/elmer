import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Container, ButtonDrop } from './styles';
import logo from './../../img/logo.png';
import imgUser from './../../img/user.png';
import Dropdown from 'react-bootstrap/Dropdown'
import PropTypes from 'prop-types';

const Header = ({ imgProfile }) => {

    return (
        <Container>
            <Link to="/apps">
                <img src={logo} width="250px" />
            </Link>
            <Dropdown>
                <ButtonDrop size="lg" id="dropdown-basic">
                    { 
                        imgProfile && 
                            <img src={imgUser} width="40" style={{ marginRight: '5%'}} /> 
                    }
                    Nelson Silva
                </ButtonDrop>
                <Dropdown.Menu>
                    <Dropdown.Item as={Link} to="/settings">Configuración</Dropdown.Item>
                    <Dropdown.Divider />
                    <Dropdown.Item>Cerrar Sesión</Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
        </Container>
    );

}

const mapStateToProps = ({ settings: { imgProfile } }) => {
    return { imgProfile };
}

Header.propTypes = {
    imgProfile: PropTypes.bool.isRequired
}

export default connect(mapStateToProps)(Header);