import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { SCButton } from './styles';
import PropTypes from 'prop-types';

const Button = ({ size, className, text}) => {

    return (
        <Row>
            <Col>
                <SCButton
                    size={size}
                    className={className}
                >
                    {text}
                </SCButton>
            </Col>
        </Row>
    );

}

Button.propTypes = {
    size: PropTypes.string.isRequired,
    className: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired
}

export default Button;