import styled from 'styled-components';
import RBButton from 'react-bootstrap/Button';

export const SCButton = styled(RBButton)`
    background-color: ${ props => props.theme.colors.secondary };
    outline: none;
    box-shadow: none;
    border: none;
`;