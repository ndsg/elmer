import React from 'react';
import { connect } from 'react-redux';
import { Container, SearchInput } from './styles';
import Row from 'react-bootstrap/Row';
import PropTypes from 'prop-types';

const Search = ({ options }) => {

    return (
        <Row className="justify-content-md-around mt-5" >
            <Container md="4">
                <SearchInput 
                    placeholder="Qué buscas?" 
                    options={options} 
                    classNamePrefix="react-select" 
                    className='react-select-container'  
                    onChange={ ({ link }) => window.open(link, '_blank') }    
                />
            </Container>
        </Row>
    );

}

const mapStateToProps = ({ apps: { data }}) => {
    let options = data.map( (item) => ( { value: item.id, label: item.name, link: item.link } ));
    return {options}
}

Search.propTypes = {
    options: PropTypes.array.isRequired
}

export default connect(mapStateToProps)(Search);