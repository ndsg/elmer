import styled from 'styled-components';
import Col from 'react-bootstrap/Col';
import Select from 'react-select';

export const Container = styled(Col)`
    background-color: ${ props => props.theme.colors.primary };
    padding: 1%;
    border-radius: 15px;
`;

export const SearchInput = styled(Select)`
    .react-select__single-value {
        color: ${ props => props.theme.colors.color };
    }
    .react-select__control {
        width: calc(100% - 50px);
        border-radius: none;
        outline: none;
        border-top: none;
        border-left: none;
        border-right: none;
        border-bottom: 2px solid ${ props => props.theme.colors.secondary };
        background-color: transparent;
        margin-left: 15px;
        padding: 2%;
        font-family: 'Regular';
        letter-spacing: 2px;
        text-transform: uppercase;
        box-shadow: none;
    }
    .react-select__control:hover{
        border-color: ${ props => props.theme.colors.secondary };
    }
    .react-select__menu{
        background-color: ${ props => props.theme.colors.primary };
    }
    .react-select__option {
        font-family: 'Regular';
        letter-spacing: 2px;
        font-size: 20px;
        background-color: ${ props => props.theme.colors.primary };
    }
    .react-select__option:hover{
        background-color: ${ props => props.theme.colors.secondary };
    }
`;