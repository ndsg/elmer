import React from 'react';
import Nav from 'react-bootstrap/Nav';
import { SCContainerTabMenu } from './styles';
import { MenuTab } from './../';
import PropTypes from 'prop-types';

const ContainerTabMenu = ({ menus }) => {

    return (
        <SCContainerTabMenu md={2}>
            <Nav variant="pills" className="flex-column">
                {
                    menus.map( (item) => 
                        <MenuTab 
                            icon={item.icon}
                            title={item.title}
                            eventKey={item.eventKey}
                            key={item.eventKey}
                        />
                    )
                }
            </Nav>
        </SCContainerTabMenu>
    );

}

ContainerTabMenu.propTypes = {
    menus: PropTypes.array.isRequired
}

export default ContainerTabMenu;