import styled from 'styled-components';
import Col from 'react-bootstrap/Col';

export const SCContainerTabMenu = styled(Col)`
    border-right: 2px solid ${ props => props.theme.colors.secondary };
    background-color: ${props => props.theme.colors.primary };
    padding: 2%;
    border-top-left-radius: 15px; 
    border-bottom-left-radius: 15px;
`;