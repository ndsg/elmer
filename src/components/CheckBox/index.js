import React from 'react';
import Form from 'react-bootstrap/Form';
import PropTypes from 'prop-types';

const CheckBox = ({ id, type, label, initialState, action }) => {

    return (
        <Form.Group controlId={id}>
            <Form.Check 
                type={type}
                label={label}
                inline
                checked={ initialState }
                onChange={ ({ target: { checked }}) => action(checked) } 
            />
        </Form.Group>
    );

}

CheckBox.propTypes = {
    id: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    initialState: PropTypes.bool.isRequired,
    action: PropTypes.func.isRequired
}

export default CheckBox;