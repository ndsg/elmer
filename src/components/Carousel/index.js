import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { CarouselItem, Spinner } from './../';
import Carousel from 'react-elastic-carousel'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import './styles.css';
import { fetch } from './../../reducers/categories';
import PropTypes from 'prop-types';

const CarouselSlide = ({ fetch, data, fetched }) => {

    useEffect( () => {
        fetch();
    }, [])

    return (
        <Row className="justify-content-md-center mt-5">
            {
                ( !fetched ) ? 
                    <Spinner />
                :
                    <Col md="11">
                        <Carousel itemsToShow={4} itemsToScroll={4}>
                            {
                                data.map( (item) => 
                                    <CarouselItem { ...item } key={item.id.toString()} />
                                )
                            }
                        </Carousel>
                    </Col>
            }
        </Row>
    );
}

const mapStateToProps = ({ categories: { data, fetched }}) => {
    return { data, fetched };
}

const mapDispatchToProps = ( dispatch ) => ({
    fetch: () => dispatch(fetch())
})

Carousel.propTypes = {
    fetch: PropTypes.func,
    data: PropTypes.object,
    fetched: PropTypes.bool
}

export default connect(mapStateToProps, mapDispatchToProps)(CarouselSlide);