import styled from 'styled-components';

export const Container = styled.div`
    background-color: ${ props => props.theme.colors.primary };
    width: 90%;
    border-radius: 15px;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: center;
    padding: 10% 0;
    &:hover {
        background-color: ${ props => props.theme.colors.secondary };
        cursor: pointer;
        transition: all 1s ease-in-out;
    }
`;

export const Title = styled.h1`
    font-family: 'Regular';
    margin-top: 5%;
    font-size: 24px;
    text-transform: uppercase;
    letter-spacing: 2px;
`;