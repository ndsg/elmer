import React from 'react';
import { Container, Title } from './styles'
import { FiMenu } from 'react-icons/fi';
import { connect } from 'react-redux';
import { showModal } from './../../reducers/modal';
import { setCategory } from './../../reducers/categories';
import { setApp } from './../../reducers/apps';
import PropTypes from 'prop-types';

const MenuItem = ({ name, id, showModal, setApp, setCategory }) => {

    return (
        <Container onClick={ () => { setApp(id); setCategory(name); showModal();  } } key={id.toString()}> 
            <FiMenu size={40} />
            <Title> {name} </Title>
        </Container>
    );

}

const mapDispatchToProps = ( dispatch ) => ({
    showModal: () => dispatch(showModal()),
    setApp: (id) => dispatch(setApp(id)),
    setCategory: (category) => dispatch(setCategory(category)),
})

MenuItem.propTypes = {
    name: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
    showModal: PropTypes.func.isRequired,
    setApp: PropTypes.func.isRequired,
    setCategory: PropTypes.func.isRequired
}

export default connect(null, mapDispatchToProps)(MenuItem);