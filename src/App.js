import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { ThemeProvider } from 'styled-components';
import { BrowserRouter as Router } from 'react-router-dom';
import { Header } from './components';
import { dark, light, chill, gold } from './styles/themes';
import { getTheme } from './reducers/settings';
import GlobalStyle from './styles/global';
import Navigation from './navigation';

const App = ({ theme, getTheme }) => {

	useEffect( () => {
		getTheme();
	}, [])

	return (
		<Router>
			<ThemeProvider theme={theme}>
				<GlobalStyle />
				<Header />
				<Navigation />
			</ThemeProvider>
		</Router>
	);
}

const mapStateToProps = ({ settings: { theme } }) => {
	let themeInfo = getThemeInfo(theme);
	return { theme: themeInfo };
}

const mapDispatchToProps = (dispatch) => ({
	getTheme: () => dispatch(getTheme())
});

const getThemeInfo = (theme) => {
	switch(theme) {
		case 'light':
			return light;
		case 'dark': 
			return dark;
		case 'chill':
			return chill;
		case 'gold':
			return gold;
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
